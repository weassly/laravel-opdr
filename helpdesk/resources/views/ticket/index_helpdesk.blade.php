@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-6">

                @if (session('success'))
                    <div class="alert alert-success">
                        {{ session('success') }}
                    </div>
                @endif

                @forelse($assigned_tickets as $ticket)

                        @can('show', $ticket)
                        <div class="card mb-3">

                            <div class="card-header">
                                {{ $ticket->submitting_user->name }}
                                <em>{{ $ticket->created_at->toFormattedDateString() }}</em>
                            </div>

                            <div class="card-body">
                                <h5 class="card-title">
                                    <a href="{{ route('ticket_show', ['id' => $ticket]) }}">{{ $ticket->title }}</a>
                                </h5>
                                <p class="card-text">
                                    {!! nl2br(e($ticket->description)) !!}
                                </p>
                            </div>

                            <div class="card-footer">
                                {{$ticket->status->description }}
                            </div>
                        </div>

                        @elsecan
                            <div class="card mb-3">

                                <div class="card-header">
                                    {{ $ticket->submitting_user->name }}
                                    <em>{{ $ticket->created_at->toFormattedDateString() }}</em>
                                </div>

                                <div class="card-body">
                                    <h5 class="card-title">
                                        {{ $ticket->title }}
                                    </h5>
                                    <p class="card-text">
                                        {!! nl2br(e($ticket->description)) !!}
                                    </p>
                                </div>

                                <div class="card-footer">
                                    {{$ticket->status->description }}
                                </div>
                            </div>
                        @endcan



                    @empty
                    {{__('No assigned tickets available...') }}
                @endforelse

            <div class="col-md-6">

                @forelse($unassigned_tickets as $ticket)


                @can('show', $ticket)
                    <div class="card mb-3">

                        <div class="card-header">
                            {{ $ticket->submitting_user->name }}
                            <em>{{ $ticket->created_at->toFormattedDateString() }}</em>
                        </div>

                        <div class="card-body">
                            <h5 class="card-title">
                                <a href="{{ route('ticket_show', ['id' => $ticket]) }}">{{ $ticket->title }}</a>
                            </h5>
                            <p class="card-text">
                                {!! nl2br(e($ticket->description)) !!}
                            </p>
                        </div>

                        <div class="card-footer">
                            {{$ticket->status->description }}
                        </div>
                    </div>

                @elsecan
                        <div class="card mb-3">

                            <div class="card-header">
                                {{ $ticket->submitting_user->name }}
                                <em>{{ $ticket->created_at->toFormattedDateString() }}</em>
                            </div>

                            <div class="card-body">
                                <h5 class="card-title">
                                    {{ $ticket->title }}
                                </h5>
                                <p class="card-text">
                                    {!! nl2br(e($ticket->description)) !!}
                                </p>
                            </div>

                            <div class="card-footer">
                                {{$ticket->status->description }}
                            </div>
                        </div>
                 @endcan


                @empty
                    {{ __('No unassigned tickets available...') }}
                @endforelse
            </div>
            </div>
        </div>
    </div>

@endsection
